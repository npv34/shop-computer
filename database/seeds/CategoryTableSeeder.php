<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->id   = 1;
        $category->name = "LAPTOP";
        $category->parent_id = "0";
        $category->save();

        $category = new Category();
        $category->id   = 2;
        $category->name = "Phụ Kiện Máy Tính ";
        $category->parent_id = "0";
        $category->save();

    }
}
