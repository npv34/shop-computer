<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->id = 1;
        $user->name = "admin";
        $user->email = "admin@ashop.com";
        $user->password = bcrypt("123456");
        $user->address = 'Hà Nội';
        $user->phone = '01656987382';
        $user->save();
    }
}
