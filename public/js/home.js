$(document).ready( function () {

   $('.updateCart').click(function () {
       var rowId = $(this).attr('id');
       var qty  = $(this).parent().parent().find('.qty').val();
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

       $.ajax({
           url : 'update-product/'+ rowId + '/' + qty,
           type: 'GET',
           cache: false,
           data:{
               "id":rowId,
               "qty":qty
           },
           success:function () {
               window.location = "shopping-cart";
           },
           error:function () {
               alert("Errors");
           }
       });
   })

    $('.updateCartCheckout').click(function () {
        var rowId = $(this).attr('id');
        var qty  = $(this).parent().parent().find('.qty').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url : 'update-product/'+ rowId + '/' + qty,
            type: 'GET',
            cache: false,
            data:{
                "id":rowId,
                "qty":qty
            },
            success:function () {
                window.location = "check-out";
            },
            error:function () {
                alert("Errors");
            }
        });
    })


});