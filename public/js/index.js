$(document).ready( function () {

    $('#data-table').DataTable();

    $('#cate_id').change(function () {
        var cate_id = $(this).val();
        getSubCategoriesByCateParentId(cate_id);
    });

    function getSubCategoriesByCateParentId(cate_id) {
        $.get(baseUrl + 'products/' + cate_id + '/getSubCategory', function (data) {

            $('#subcate_id').html(data);
        });
    }

    $(".reveal").on('click',function() {
        var $pwd = $(".pwd");
        if ($pwd.attr('type') === 'password') {
            $pwd.attr('type', 'text');
        } else {
            $pwd.attr('type', 'password');
        }
    });

    $('#addImage').click(function () {
        $('#insert-img').append('<div class="form-group"><input id="uploadImage" type="file" name="imageProductDetail[]" class="form-control-file"></div>');
    })
});

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
};

function goBack() {
    window.history.back();
}


