<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ProductRequests;
use App\Product;
use App\ProductImages;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function create()
    {
        $categories = Category::all();
        return view('layouts.admin.product.add',compact('categories', 'subCategories'));
    }

    public function store(ProductRequests $request)
    {
        $product        = new Product();
        $product->name  = $request->input('name');
        $product->code  = $request->input('code');
        $product->price = $request->input('price');
        $product->promotion_price   = $request->input('promotion_price');
        $product->quantity   = $request->input('quantity');
        $product->description       = $request->input('desc');
        $product->content           = $request->input('content');

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('images/products', 'public');
            $product->image = $path;
        }
        $product->status        = $request->input('status');
        $product->category_id   = $request->input('cate_id');
        $category = Category::find($request->input('cate_id'));
        $product->category_parent_id   = $category->parent_id;
        $product->save();

        if ($request->hasFile('image_detail')) {
            $arrImagesDeatil = $request->file('image_detail');
            foreach($arrImagesDeatil as $imageDetail) {
                $productIamge  = new ProductImages();
                $path = $imageDetail->store('images/products', 'public');
                $productIamge->image        = $path;
                $productIamge->product_id   = $product->id;
                $productIamge->save();
            }
        }
        Session::flash('success', 'Add new success!');
        return redirect()->route('admin.products.list');
    }

    public function showList()
    {
        $products = Product::all();
        return view('layouts.admin.product.list', compact('products'));
    }

    public function delete($productId)
    {
        $product = $this->getProductById($productId);
        return view('layouts.admin.product.delete', compact('product'));
    }

    public function destroy($productId)
    {
        $product = $this->getProductById($productId);
        foreach ($product->images as $imageDetail) {
            Storage::disk('public')->delete($imageDetail->image);
        }
        Storage::disk('public')->delete($product->image);
        $product->delete();
        Session::flash('success', 'Delete success!');
        return redirect()->route('admin.products.list');
    }

    /**
     * @param $productId
     * @return mixed
     */
    private function getProductById($productId)
    {
        $product = Product::find($productId);
        if (!$product) {
            abort(404);
        }
        return $product;
    }

    public function update($productId)
    {
        $product = $this->getProductById($productId);
        $categories = Category::all();
        return view('layouts.admin.product.edit', compact('product', 'categories'));
    }

    public function edit(Request $request, $productId)
    {
        $product = $this->getProductById($productId);
        $product->name  = $request->input('name');
        $product->code  = $request->input('code');
        $product->price = $request->input('price');
        $product->promotion_price   = $request->input('promotion_price');
        $product->quantity   = $request->input('quantity');
        $product->description       = $request->input('desc');
        $product->content           = $request->input('content');

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($product->image);
            $image = $request->file('image');
            $path = $image->store('images/products', 'public');
            $product->image = $path;
        }
        $product->status        = $request->input('status');
        $product->category_id   = $request->input('cate_id');
        $category = Category::find($product->category_id);
        $product->category_parent_id   = $category->parent_id;
        $product->save();
        Session::flash('success', 'Update success!');
        return redirect()->route('admin.products.list');

    }

}
