<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillDetail;
use App\Guest;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShoppingCartController extends Controller
{
    public function storeCart(Request $request)
    {
        try {
            $guest = new Guest();
            $guest->first_name = $request->input('first_name');
            $guest->last_name = $request->input('last_name');
            $guest->email = $request->input('email_guest');
            $guest->phone = $request->input('phone_guest');
            $guest->address = $request->input('address_guest');
            $guest->save();

            $bill = new Bill();
            $bill->guest_id = $guest->id;
            $total = Cart::subtotal(0,',','.');
            $bill->total    = $total;
            $bill->status   = "0";
            $bill->save();

            $cartInfor = Cart::content();

            if (count($cartInfor) >0) {
                foreach ($cartInfor as $key => $item) {
                    $billDetail = new BillDetail();
                    $billDetail->bill_id = $bill->id;
                    $billDetail->product_id = $item->id;
                    $billDetail->quantily = $item->qty;
                    $billDetail->price = $item->price;
                    $billDetail->save();
                }
            }
            Cart::destroy();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        Session::flash('success','Mua thành công! Cảm ơn bạn đã tin dùng ashop.local');

        return redirect()->back();
    }

    public function showGuests()
    {
        $guests = Guest::all();
        return view('layouts.admin.guests.list',compact('guests'));
    }

    public function showBills()
    {
        $bills = Bill::all();
        return view('layouts.admin.bills.list',compact('bills'));
    }

    public function showBillDetail($billId)
    {
        $billDetails = BillDetail::with('bill','product')
                        ->where('bill_id',$billId)->get();
        $bill = Bill::find($billId);

        return view('layouts.admin.bills.detail',compact('billDetails','bill'));
    }

    public function updateBill(Request $request, $billId)
    {
        $bill = Bill::find($billId);
        if ($bill) {
            abort(404);
        }
        $bill->status = $request->input('status');
        $bill->save();
        Session::flash('success', 'Update success!');
        return redirect()->route('admin.showBills');
    }

    public function destroyBill(Request $request,$billId)
    {
        try {
            $bill = Bill::find($billId);
            if ($bill) {
                abort(404);
            }

            $billDetails = BillDetail::with('bill','product')
                ->where('bill_id',$billId)->get();

            foreach ($billDetails as $billDetail) {
                $billDetail->delete();
            }
            $bill->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        Session::flash('success', 'Delete success!');
        return redirect()->route('admin.showBills');
    }
}
