<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function showList()
    {
        $users = User::all();
        return view('layouts.admin.user.list', compact('users'));
    }

    public function create()
    {
        return view('layouts.admin.user.add');
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->save();
        Session::flash('success', 'add new user success!');
        return redirect()->route('admin.users');
    }

    public function update($user_id)
    {

        $user = User::find($user_id);
        if (!$user || $user->id == UserConstant::ADMIN) {
            abort(404);
        }
        return view('layouts.admin.user.edit', compact('user'));
    }

    public function edit(Request $request, $user_id)
    {
        $user = User::find($user_id);
        if (!$user || $user->id == UserConstant::ADMIN) {
            abort(404);
         }
        $user->name = $request->input('name');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->save();
        Session::flash('success', 'update user success!');
       return redirect()->route('admin.users');
    }

    public function destroy($user_id)
    {
        $user = User::find($user_id);
        if (!$user || $user->id == UserConstant::ADMIN) {
            abort(404);
        }
        $user->delete();
        Session::flash('success', 'Delete user success!');
        return redirect()->route('admin.users');
    }

}
