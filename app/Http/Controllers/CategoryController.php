<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function create()
    {
        $cateParents = Category::all();
        return view('layouts.admin.categories.add', compact('cateParents'));
    }

    public function store(Request $request)
    {
        $cate = new Category();
        $cate->name = $request->input('name');
        $cate->parent_id  = $request->input('cate-parent');
        $cate->description = $request->input('desc');
        Session::flash('success', 'Add new success!');
        $cate->save();
        return redirect()->route('admin.categories.list');
    }

    public function edit($cate_id)
    {
        $category = Category::find($cate_id);
        if (!$category) {
            return abort(404);
        }
        $cateParents = Category::all();
        return view('layouts.admin.categories.edit', compact('category','cateParents'));
    }

    public function update(Request $request, $cate_id)
    {
        $category = Category::find($cate_id);
        if (!$category) {
            return abort(404);
        }
        $category->name = $request->input('name');
        $category->parent_id  = $request->input('cate-parent');
        $category->description = $request->input('desc');
        Session::flash('success', 'Update success!');
        $category->save();
        return redirect()->route('admin.categories.list');
    }

    public function showList()
    {
        $categories = Category::all();
        return view('layouts.admin.categories.list', compact('categories'));
    }

    public function delete($cate_id)
    {
        $category = Category::find($cate_id);
        if (!$category) {
            return abort(404);
        }
        $cateChild = Category::where('parent_id',$cate_id)->get();
        if (count($cateChild) > 0) {
            Session::flash('can_not_delete', 'This category exists in the element!');
            return view('layouts.notification.no-delete');
        }
        return view('layouts.admin.categories.delete', compact('category'));
    }

    public function destroy($cate_id)
    {
        $category = Category::find($cate_id);
        if (!$category) {
            return abort(404);
        }
        $this->hashProductIntoCategory($cate_id);
        $this->checkCategoryChild($cate_id);
        $category->delete();
        Session::flash('success', 'Delete success!');
        return redirect()->route('admin.categories.list');
    }

    public function checkCategoryChild($cate_id)
    {
        $cateChild = Category::where('parent_id',$cate_id)->get();
        if (count($cateChild) > 0) {
            Session::flash('can_not_delete', 'This category exists in the element!');
            return view('layouts.notification.no-delete');
        }
    }

    public function hashProductIntoCategory($cate_id)
    {
        $category = Category::with('products')->find($cate_id);
        if (count($category->products) > 0) {
            Session::flash('can_not_delete', 'Category is currently in the products');
            return view('layouts.notification.no-delete');
        }
    }
}
