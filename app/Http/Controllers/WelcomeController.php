<?php

namespace App\Http\Controllers;


use Cart;
use App\Category;
use App\Product;
use Illuminate\Http\Request;


class WelcomeController extends Controller
{

    public function index()
    {
        $productsFeatured = Product::where('status','=', 'Show')->orderBy('price')->get();
        $productsLatest   = Product::where('status','=', 'Show')->orderBy('created_at','DESC')->get();
        return view('home',compact('productsFeatured','productsLatest'));
    }

    public function getCategory($slugCategory)
    {
        $category = Category::where('slug','=',$slugCategory)->first();
        if (!$category) {
            abort(404);
        }
        $productsCategory = Product::where('status','=', 'Show')
                            ->where('category_id','=',$category->id)
                            ->orWhere('category_parent_id',$category->id)
                            ->paginate(6);

        $productsLatest   = Product::where('status','=', 'Show')
                            ->where('category_id','=',$category->id)
                            ->orWhere('category_parent_id',$category->id)
                            ->orderBy('id','DESC')->take(3)->get();
        $menusCate = Category::where('parent_id',$category->id)->get();
        return view('layouts.page.category',compact('productsCategory','menusCate','category','productsLatest'));
    }

    public function getProductDetail($slugCategory, $slugProduct)
    {
        $product = Product::where('slug','=',$slugProduct)->first();;
        if (!$product) {
            abort(404);
        }
        $productsRelate = Product::where('category_id',$product->category_id)
                        ->where('id','<>',$product->id)
                        ->take(4)
                        ->get();

        return view('layouts.page.product',compact('product','productsRelate'));
    }

    public function shopping($productId)
    {
        $productBy = $this->getProductById($productId);
        if (!$productBy) {
            abort(404);
        }
        if ($productBy->promotion_price) {
            $priceProduct = $productBy->promotion_price;
        } else {
            $priceProduct = $productBy->price;
        }
        Cart::add([ 'id'    =>  $productId,
                    'name'  =>  $productBy->name,
                    'qty'   =>  1,
                    'price' =>  $priceProduct,
                    'options'   =>  [
                        'img'   =>$productBy->image,
                    ]
        ]);
        return redirect()->route('user.showCart');
    }

    public function showCart()
    {
        $contentCart =Cart::content();
        $total = Cart::subtotal(0,',','.');

        return view('layouts.page.shopping-cart',compact('contentCart','total'));
    }

    public function removeProductCart($productId)
    {
        Cart::remove($productId);
        return redirect()->route('user.showCart');
    }

    /**
     * @param $productId
     * @return mixed
     */
    private function getProductById($productId)
    {
        $productBy = Product::find($productId);
        return $productBy;
    }

    public function updateProductCart(Request $request)
    {
        if ($request->ajax()) {
            $id     = $request->id;
            $qty    = $request->qty;
            Cart::update($id,$qty);
        }
    }

    public function checkout()
    {
        $contentCart =Cart::content();
        $total = Cart::subtotal(0,',','.');
        return view('layouts.page.checkout',compact('contentCart','total'));
    }
}
