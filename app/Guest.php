<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = "guest";

    public function bills()
    {
        return $this->hasMany('App\Bill');
    }
}
