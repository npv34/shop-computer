<?php
/**
 * Created by PhpStorm.
 * User: phanluan
 * Date: 11/05/2018
 * Time: 22:16
 */

namespace App\Helpers;


class helpers
{
    public static function getCategoryParent($data, $parent = 0, $str = "--", $select = 0)
    {
        foreach ($data as $value) {
            $id = $value->id;
            $name = $value->name;
            $parentId = $value->parent_id;
            if ($parentId == $parent) {
                if ($select != 0 && $id == $select) {
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
                self::getCategoryParent($data, $id, $str."--",$select);
            }

        }
    }
}