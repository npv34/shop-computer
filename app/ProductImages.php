<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = "images_detail_product";

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
