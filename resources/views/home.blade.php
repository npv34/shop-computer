@extends('welcome')

@section('home-content')

<section id="featured" class="row mt40">
    <div class="container">
        <h1 class="heading1"><span class="maintext">Sản phẩm nổi bật</span><span class="subtext"></span></h1>
        <ul class="thumbnails">
            @foreach($productsFeatured as $product)
            <li class="col-lg-3  col-sm-6">
                <a class="prdocutname" href="{{ route('user.getProductDetail',[$product->category->slug,$product->slug] ) }}">{{ $product->name }}</a>
                <div class="thumbnail">
                    @if($product->promotion_price)
                    <span class="sale tooltip-test">Sale</span>
                    @endif
                    <a href="{{ route('user.getProductDetail',[$product->category->slug,$product->slug] ) }}"><img alt="" src="{{ ($product->image) ? asset('storage/'.$product->image) : '' }}"></a>
                    <div class="shortlinks">
                        <a class="details" href="#">DETAILS</a>
                        <a class="wishlist" href="#">WISHLIST</a>
                        <a class="compare" href="#">COMPARE</a>
                    </div>
                    <div class="pricetag">
                        <span class="spiral"></span><a href="{{ route('user.shopping',[$product->id,$product->slug]) }}" class="productcart">ADD TO CART</a>
                        <div class="price">
                            @if(count($product->promotion_price) > 0)
                                <div class="pricenew">{{ number_format($product->promotion_price,0,",",".") }}</div>
                                <div class="priceold">{{ number_format($product->price,0,",",".") }}</div>
                            @else
                                <div class="pricenew">{{ number_format($product->price,0,",",".") }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </li>
            @endforeach

        </ul>
    </div>
</section>

<!-- Latest Product-->
<section id="latest" class="row">
    <div class="container">
        <h1 class="heading1"><span class="maintext">Sản Phẩm Mới Nhất </span></h1>
        <ul class="thumbnails">
            @foreach($productsLatest as $productLatest)
            <li class="col-lg-3 col-sm-6">
                <a class="prdocutname" href="{{ route('user.getProductDetail',[$productLatest->category->slug,$productLatest->slug] ) }}">{{ $productLatest->name }}</a>
                <div class="thumbnail">
                    <a href="{{ route('user.getProductDetail',[$productLatest->category->slug,$productLatest->slug] ) }}"><img alt="" src="{{ ($productLatest->image) ? asset('storage/'.$productLatest->image) : '' }}"></a>
                    <div class="shortlinks">
                        <a class="details" href="#">DETAILS</a>
                        <a class="wishlist" href="#">WISHLIST</a>
                        <a class="compare" href="#">COMPARE</a>
                    </div>
                    <div class="pricetag">
                        <span class="spiral"></span><a href="{{ route('user.shopping',[$productLatest->id,$productLatest->slug]) }}" class="productcart">ADD TO CART</a>
                        <div class="price">
                            @if(count($productLatest->promotion_price) > 0)

                            <div class="pricenew">{{ number_format($productLatest->promotion_price,0,",",".") }}</div>
                            <div class="priceold">{{ number_format($productLatest->price,0,",",".") }}</div>
                             @else
                                <div class="priceold">{{ number_format($productLatest->price,0,",",".") }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</section>

<!-- Section  Banner Start-->
<section class="container smbanner">
    <div class="row">
        <div class="col-lg-3 col-sm-6"><a href="#"><img src="img/smbanner.jpg" alt="" title=""></a>
        </div>
        <div class="col-lg-3 col-sm-6"><a href="#"><img src="img/smbanner.jpg" alt="" title=""></a>
        </div>
        <div class="col-lg-3 col-sm-6"><a href="#"><img src="img/smbanner.jpg" alt="" title=""></a>
        </div>
        <div class="col-lg-3 col-sm-6"><a href="#"><img src="img/smbanner.jpg" alt="" title=""></a>
        </div>
    </div>
</section>
<!-- Section  End-->

<!-- Popular Brands-->
<section id="popularbrands" class="container mt40">
    <h1 class="heading1"><span class="maintext">Thương hiệu phổ biến</span></h1>
    <div class="brandcarousalrelative">
        <ul id="brandcarousal">
            <li><img src="{{asset('img/brand1.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand2.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand3.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand4.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand1.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand2.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand3.jpg')}}" alt="" title=""/></li>
            <li><img src="{{asset('img/brand4.jpg')}}" alt="" title=""/></li>
        </ul>
        <div class="clearfix"></div>
        <a id="prev" class="prev" href="#">&lt;</a>
        <a id="next" class="next" href="#">&gt;</a>
    </div>
</section>

<!-- Newsletter Signup-->
<section id="newslettersignup" class="mt40">
    <div class="container">
        <div class="pull-left newsletter">
            <h2> Newsletters Signup</h2>
            Đăng ký nhận bản tin của chúng tôi và nhận ưu đãi hấp dẫn bằng cách đăng ký nhận bản tin của chúng tôi.</div>
        <div class="pull-right">
            <form class="form-horizontal">
                <div class="input-prepend">
                    <input type="text" placeholder="Subscribe to Newsletter" id="inputIcon" class="input-xlarge">
                    <input value="Subscribe" class="btn btn-orange" type="submit">
                    Sign in </div>
            </form>
        </div>
    </div>
</section>
<!-- Featured Product-->
@endsection