<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Shop Computer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=100" >
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" rel="stylesheet"  />
    <link href="{{asset('css/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('css/cloud-zoom.css')}}" rel="stylesheet">
    <link href="{{asset('css/portfolio.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- fav -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>
<body>
<!-- Header Start -->
@include('layouts.block.header')
<!-- Header End -->
    <!-- Slider Start-->
    @include('layouts.block.slider')
    <!-- Slider End-->

    <!-- Section Start-->
    @include('layouts.block.ortherdetail')
    <!-- Section End-->
    @yield('home-content')
<!-- /maincontainer -->

<!-- Footer -->
@include('layouts.block.footer')
<!-- javascript
    ================================================== -->

<!-- Load Facebook SDK for JavaScript -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/home.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/respond.min.js')}}"></script>
<script src="{{asset('js/application.js')}}"></script>
<script src="{{asset('js/bootstrap-tooltip.js')}}"></script>
<script defer src="{{asset('js/jquery.fancybox.js')}}"></script>
<script defer src="{{asset('js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.tweet.js')}}"></script>
<script  src="{{asset('js/cloud-zoom.1.0.2.js')}}"></script>
<script  type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery.carouFredSel-6.1.0-packed.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery.mousewheel.min.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery.touchSwipe.min.js')}}"></script>
<script type="text/javascript"  src="{{asset('js/jquery.ba-throttle-debounce.min.js')}}"></script>
<script src="{{asset('js/jquery.isotope.min.js')}}"></script>
<script defer src="{{asset('js/custom.js')}}"></script>
</body>
</html>