<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <title>{{__('login.login')}}</title>

</head>

<body class="bg-dark">

<div class="container">

    <div class="card card-login mx-auto mt-5">
        <div class="card-header">
            {{ Lang::get('login.login') }}
        </div>
        <div class="card-body">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">{{ Lang::get('login.E-Mail_Address') }}</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder=""
                           aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                           <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? 'errors' : '' }}">
                    <label for="password">{{ Lang::get('login.password') }}</label>
                    <input id="password" type="password" class="form-control" name="password"
                           placeholder="" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input"
                                   name="remember" {{ old('remember') ? 'checked' : '' }}> {{ Lang::get('login.remember_me') }}
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    {{ Lang::get('login.login') }}
                </button>
            </form>
        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('js/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

</body>

</html>


{{--@endsection--}}
