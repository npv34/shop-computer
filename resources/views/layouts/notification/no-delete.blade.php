@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Delete Categorie</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="col-12 row pt-5">
                        <div class="col-12 col-md-3">
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Can Not Delete</h5>
                                </div>
                                <div class="modal-body">
                                    <p class="text-danger">
                                        @if(Session::has('can_not_delete'))
                                            {{ Session::get('can_not_delete') }}
                                        @endif
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn text-danger" onclick="goBack()">Close</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection