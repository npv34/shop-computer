@if(Session::has('error'))
    <p class="text-danger" style="">
        <i class="fa fa-check" aria-hidden="true"></i>{{ Session::get('error')}}
    </p>
@endif