<div id="categorymenu">
    <nav class="subnav">
        <ul class="nav-pills categorymenu">
            <li><a class="active"  href="{{ url('/') }}"><i class="fa fa-home"></i> Trang chủ </a></li>
            <li><a  href="#"><i class="fa fa-navicon"></i> Danh mục sản phẩm </a>
                <div>
                    <ul>
                        <?php
                            $categories = \App\Category::where('parent_id', 0)->get();
                        ?>
                        @foreach($categories as $category)
                        <li><a href="{{route('user.getCategory',$category->slug)}}">{{ $category->name }}</a>
                            <?php
                            $subCategories = \App\Category::where('parent_id', $category->id)->get();
                            ?>
                            @if(count($subCategories) > 0)
                            <div>
                                <ul>
                                    @foreach($subCategories as $subCategory)
                                    <li><a href="{{route('user.getCategory',[$subCategory->slug])}}">{{ $subCategory->name }}</a> </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            <li><a href="{{ route('user.showCart') }}"><i class="fa fa-shopping-cart"></i> Giỏ hàng </a> </li>
            <li><a href="{{ route('user.checkout') }}"><i class="fa fa-cart-arrow-down"></i> Thanh toán</a> </li>
            <li><a href=""><i class="fa fa-globe"></i> Blog</a>
                <div>
                    <ul>
                        <li><a href="">Blog page</a> </li>
                        <li><a href="">Blog List VIew</a> </li>
                    </ul>
                </div>
            </li>
            <li><a href=""><i class="fa fa-gift"></i> Khuyến mãi</a>
            </li>
            <li><a href=""><i class="fa fa-envelope-o"></i> Liên hệ </a>
            </li>
        </ul>
    </nav>
</div>