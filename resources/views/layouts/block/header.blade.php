<header>
    <div class="headerstrip">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"> <a href="{{ url('/') }}" class="logo pull-left"><img src="{{asset('img/logo.png')}}" alt="SimpleOne" title="SimpleOne"></a>
                    <!-- Top Nav Start -->
                    <div class="pull-left">
                        <div class="navbar" id="topnav">
                            <div class="navbar-inner">
                                <ul class="nav" >
                                    <li><a class="home active" href="{{ url('/') }}">Trang chủ </a> </li>
                                    <li><a class="shoppingcart" href="{{ route('user.showCart') }}">Giỏ hàng</a> </li>
                                    <li><a class="checkout" href="#">Thanh toán</a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav End -->
                    <div class="pull-right">
                        <form class="form-search top-search">
                            <input type="text" class="input-medium search-query" placeholder="Search Here…">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="headerdetails">
            <div class="pull-left">
                <ul class="nav language pull-left">
                    <li class="dropdown hover"> <a href="#" class="dropdown-toggle" data-toggle="">US Doller <b class="caret"></b></a>
                        <ul class="dropdown-menu currency">
                            <li><a href="#">US Doller</a> </li>
                            <li><a href="#">Euro </a> </li>
                            <li><a href="#">British Pound</a> </li>
                        </ul>
                    </li>
                    <li class="dropdown hover"> <a href="#" class="dropdown-toggle" data-toggle="">English <b class="caret"></b></a>
                        <ul class="dropdown-menu language">
                            <li><a href="#">English</a> </li>
                            <li><a href="#">Spanish</a> </li>
                            <li><a href="#">German</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <ul class="nav topcart pull-left">
                    <li class="dropdown hover carticon ">
                        <a href="{{ route('user.showCart') }}" class="dropdown-toggle" > Giỏ hàng
                            <span class="label label-orange font14">{{ Cart::count() }} sản phẩm(s)
                            </span> - {{Cart::subtotal(0,',','.')}} VNĐ
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu topcartopen ">
                            <li>
                                <table>
                                    <tbody>
                                    @foreach(Cart::content() as $item)
                                    <tr>
                                        <td class="image" style="width: 100px"><a href=""><img width="50" height="50" src="{{ asset('storage/'. $item->options->img ) }}" alt="product" title="product"></a></td>
                                        <td class="name"><a href="">{{ $item->name }}</a></td>
                                        <td class="quantity">x{{ $item->qty }}</td>
                                        <td class="total">{{ number_format($item->price * $item->qty,0,",",".") }} VNĐ</td>
                                        <td class="remove"><i class="icon-remove"></i></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="textright">
                                            <b>Tổng tiền:</b>
                                            <p>Chưa bao gồm VAT(10%)</p>
                                        </td>
                                        <td class="textright">{{Cart::subtotal(0,',','.')}} VNĐ</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="well pull-right buttonwrap"> <a class="btn btn-orange" href="{{ url('/shopping-cart') }}">Giỏ hàng </a> <a class="btn btn-orange" href="#">Thanh toán</a> </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        @include('layouts.block.nav')
    </div>
</header>