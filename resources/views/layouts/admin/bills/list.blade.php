@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Bills</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    @include('layouts.notification.success')
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <table id="data-table" class="table">
                            <thead class="">
                            <tr>
                                <th scope="col">MA</th>
                                <th scope="col">Guest</th>
                                <th scope="col">Total</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date order</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bills as $key => $bill)
                                <tr>
                                    <th scope="row"><a href="{{ route('admin.showBillDetail',$bill->id) }}">HD-{{ ++$key }}</a></th>
                                    <td>{{ $bill->guest->first_name}} {{ $bill->guest->last_name}}</td>
                                    <td>{{ $bill->total }} VNĐ</td>
                                    @if($bill->status == '0')
                                        <td><span>Đơn hàng mới</span></td>
                                    @elseif($bill->status == '1')
                                        <td><span>Chưa giao</span></td>
                                    @elseif($bill->status == '2')
                                        <td><span>Đang giao</span></td>
                                    @else
                                        <td><span>Đã giao</span></td>
                                    @endif
                                    <td>{{ $bill->created_at }}</td>
                                    <td class="text-center">
                                        <div class="ml-auto">
                                            <a class="pt-2 pl-3" href="{{ route('admin.showBillDetail',$bill->id) }}"><i class="fa fa-eye"></i>Detail </a>
                                            <a class="pt-2 pl-3" href="" data-toggle="modal" data-target="#modalDeleteBill"><i class="fa fa-close text-danger"></i>Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <form method="post" action="{{ route('admin.destroyBill', $bill->id) }}">
        {{ csrf_field() }}
        <div class="modal fade" id="modalDeleteBill" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalDeleteBill">Delete Bill</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="text-danger">Are you sure you want to delete this bill?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Delete</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
@endsection