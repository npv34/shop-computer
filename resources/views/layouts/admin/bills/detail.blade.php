@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Bills Detail</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 pb-2">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Information</h3>
                    </div>
                    <div class="guest-detail col-6">
                        <div class="col-12 row">
                            <div class="col-4">
                                Name Guest:
                            </div>
                            <div class="col-8">
                                <span>{{ $bill->guest->first_name }} {{ $bill->guest->last_name }}</span>
                            </div>
                        </div>
                        <div class="col-12 row">
                            <div class="col-4">
                                Date order:
                            </div>
                            <div class="col-8">
                                <span>{{ $bill->created_at }}</span>
                            </div>
                        </div>
                        <div class="col-12 row">
                            <div class="col-4">
                                Email:
                            </div>
                            <div class="col-8">
                                <span>{{ $bill->guest->email }}</span>
                            </div>
                        </div>
                        <div class="col-12 row">
                            <div class="col-4">
                                Phone:
                            </div>
                            <div class="col-8">
                                <span>{{ $bill->guest->phone }}</span>
                            </div>
                        </div>
                        <div class="col-12 row">
                            <div class="col-4">
                                Address:
                            </div>
                            <div class="col-8">
                                <span>{{ $bill->guest->address }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="image">Ảnh </th>
                        <th class="name">Tên sản phẩm </th>
                        <th class="quantity">Số lượng </th>
                        <th class="price">Giá </th>
                        <th class="total">Thành tiền </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($billDetails as $key => $billDetail)
                    <tr>
                        <td>
                            <img title="product" alt="product" src="{{ asset('storage/'. $billDetail->product->image ) }}" height="50" width="50">
                        </td>
                        <td>{{ $billDetail->product->name }}</td>
                        <td>{{ $billDetail->quantily}} </td>
                        <td>{{ number_format($billDetail->price,0,',','.')}} VNĐ</td>
                        <td>{{ number_format($billDetail->price * $billDetail->quantily,0,",",".") }} VNĐ</td>
                    </tr>
                    @endforeach
                    <tr>
                        <th colspan="4" scope="row">
                            Tổng tiền
                        </th>
                        <td>
                            <span class="text-danger">{{ $bill->total }} VNĐ</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <form action="{{ route('admin.updateBill',$bill->id) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-3 col-form-label">Trạng thái giao hàng </label>
                        <div class="col-sm-4">
                            <select id="inputStatus" name="status" class="form-control">
                                <option value="0">Đơn hàng mới</option>
                                <option value="1">Chưa giao</option>
                                <option value="2">Đang giao</option>
                                <option value="3">Đã giao</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">Apply</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection