@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Products</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <form method="post" action="{{route('admin.products.destroy',$product->id)}}">
                        {{ csrf_field() }}
                        <div class="col-12 row pt-5">
                            <div class="col-12 col-md-3">
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Delete Product: {{$product->name}}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-danger">Are you sure you want to delete this product?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-secondary" href="{{ route('admin.products.list') }}">Close</a>
                                        <button type="submit" class="btn btn-primary" >Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection