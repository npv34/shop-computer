@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Products</li>
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Create new product</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form action="{{route('admin.product.store')}}" method="post" enctype="multipart/form-data">
                    <div class="card row">
                    <div class="card-header">
                        <h3 class="mb-0">Information</h3>
                    </div>
                    <div class="row">
                        <div class="col-7 col-md-7 card-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleSelect1">Category Parent</label>
                                    <span class="align-middle text-danger">*</span>
                                    <select class="form-control" name="cate_id" id="cate_id">
                                        <option>Please Choose Category</option>
                                        <?php \App\Helpers\helpers::getCategoryParent($categories) ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="name" class="form-control" value="{{old('name')}}" required>
                                </div>
                                <div class="form-group">
                                    <label>Code</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="code" class="form-control" value="{{old('code')}}" required>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="price" class="form-control" value="{{ old('price') }}"  required>
                                </div>
                                <div class="form-group">
                                    <label>Promotion Price</label>
                                    <input type="text" name="promotion_price" class="form-control" value="{{ old('promotion_price') }}">
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="quantity" class="form-control" value="{{ old('quantity') }}"  required>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <span class="align-middle text-danger">*</span>
                                    <textarea type="text" name="desc" class="form-control" required>{{ old('desc') }}</textarea>
                                    <script>
                                        CKEDITOR.replace( 'desc' );
                                    </script>
                                </div>
                                <div class="form-group">
                                    <label>Content</label>
                                    <span class="align-middle text-danger">*</span>
                                    <textarea type="text" name="content" class="form-control" required>{{ old('content') }}</textarea>
                                    <script>
                                        CKEDITOR.replace( 'content' );
                                    </script>
                                </div>
                                <div class="form-group">
                                    <img id="uploadPreview" style="width: 200px; height: 200px;" />
                                    <label>Images</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input  id="uploadImage" type="file" name="image" onchange="PreviewImage();" class="form-control-file">
                                    <small id="fileHelp" class="form-text text-muted">Image product</small>
                                </div>
                                <fieldset class="form-group">
                                    <label>Show/Hide</label>
                                    <span class="align-middle text-danger">*</span>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="Show" checked>
                                            Show
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="Hide" checked>
                                            Hide
                                        </label>
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <div class="col-5 col-md-5 card-body" >
                            @for($i = 1; $i <= 4; $i++)
                            <div class="form-group">
                                <label>Images Product Detail {{$i}}</label>
                                <input  id="uploadImageDetail" type="file" name="image_detail[]" class="form-control-file">
                                <small id="fileHelp" class="form-text text-muted">Image product detail</small>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection