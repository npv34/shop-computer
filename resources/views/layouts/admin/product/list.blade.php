@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Products</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="card-header">
                        <a class="btn btn-primary float-left" href="{{route('admin.product.new')}}">Create</a>
                    </div>
                    @include('layouts.notification.success')
                    @include('layouts.notification.error')
                    <div class="col-12 col-md-12 card-body table-responsive">
                            <table id="data-table" class="table">
                                <thead class="">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" width="200">Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Parent</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $product)
                                    <tr>
                                        <th scope="row">{{ ++$key }}</th>
                                        <td>{{ $product->name}}</td>
                                        <td>{{ $product->price }} VND</td>
                                        <td>
                                            <div class="d-inline-block">
                                                <img id="img-preview" class="d-block img-thumbnail img-responsive mb-2"
                                                     src="{{ ($product->image) ? asset('storage/'.$product->image) : '' }}" width="100">
                                            </div>
                                        </td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>{{ $product->status }}</td>
                                        <td class="text-center">
                                            <div class="ml-auto">
                                                <a class="pt-2 pl-3" href="{{route('admin.products.update',$product->id)}}"><i class="fa fa-edit"></i>Edit </a>
                                                <a class="pt-2 pl-3" href="{{route('admin.products.delete',$product->id)}}" ><i class="fa fa-close text-danger"></i>Delete</a>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

@endsection