@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Products</li>
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Update product</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form action="{{route('admin.products.edit',$product->id)}}" method="post" enctype="multipart/form-data">
                    <div class="card row">
                        <div class="card-header">
                            <h3 class="mb-0">Information</h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-7 card-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleSelect1">Category Parent</label>
                                    <span class="align-middle text-danger">*</span>
                                    <select class="form-control" name="cate_id" id="cate_id">
                                        <option>Please Choose Category</option>
                                        <?php \App\Helpers\helpers::getCategoryParent($categories, 0, "--", $product->category_id) ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="name" class="form-control" value="{{$product->name}}" required>
                                </div>
                                <div class="form-group">
                                    <label>Code</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="code" class="form-control" value="{{$product->code}}" required>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="price" class="form-control" value="{{ $product->price }}"  required>
                                </div>
                                <div class="form-group">
                                    <label>Promotion Price</label>
                                    <input type="text" name="promotion_price" class="form-control" value="{{ $product->promotion_price }}">
                                </div>
                                <div class="form-group">
                                    <label>Quantity</label>
                                    <span class="align-middle text-danger">*</span>
                                    <input type="text" name="quantity" class="form-control" value="{{ $product->quantity }}"  required>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <span class="align-middle text-danger">*</span>
                                    <textarea type="text" name="desc" class="form-control" required>{!! $product->description !!}</textarea>
                                    <script>
                                        CKEDITOR.replace( 'desc' );
                                    </script>
                                </div>
                                <div class="form-group">
                                    <label>Content</label>
                                    <span class="align-middle text-danger">*</span>
                                    <textarea type="text" name="content" class="form-control" required>{!! $product->content !!}</textarea>
                                    <script>
                                        CKEDITOR.replace( 'content' );
                                    </script>
                                </div>
                                <div class="form-group">
                                    <div class="col-12">
                                        <label>Image Current</label>
                                    </div>
                                    <img class="border" src="{{($product->image) ? asset('storage/'.$product->image) : ''}}" alt="{{$product->name}}" width="200">
                                </div>

                                <div class="form-group">
                                    <div class="col-12">
                                        <label>Image New</label>
                                    </div>
                                    <img id="uploadPreview" style="width: 200px; height: 200px;" />
                                    <input id="uploadImage" type="file" name="image" onchange="PreviewImage();" class="form-control-file">
                                    <small id="fileHelp" class="form-text text-muted">Image product</small>
                                </div>
                                <fieldset class="form-group">
                                    <label>Show/Hide</label>
                                    <span class="align-middle text-danger">*</span>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="Show" @if($product->status == 'Show')  checked ="checked" @endif>
                                            Show
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="optionsRadios1" value="Hide"  @if($product->status == 'Hide')  checked ="checked" @endif >
                                            Hide
                                        </label>
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="col-12 col-md-5 card-body" >
                                @foreach($product->images as $key=> $imageDetail)
                                    <div class="form-group">
                                        <div class="img-detail">
                                            <img class="border" id="image-detail-{{$key}}" src="{{($imageDetail->image) ? asset('storage/'.$imageDetail->image) : ''}}" alt="" width="200">
                                            <a href="" id="del-image-detail-{{$key}}" class="btn btn-danger icon_del_image"><i class="fa fa-close"></i></a>
                                            <input type="file" name="imageProductDetail[]">
                                        </div>
                                    </div>
                                @endforeach
                                    <a class="btn btn-primary mb-2 text-white" id="addImage">Add Image</a>
                                    <div id="insert-img"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection