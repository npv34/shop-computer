@extends('layouts.app')

@section('content')



<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @include('layouts.admin.cards')
                    @include('layouts.admin.charts')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
