@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Users</li>
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Update</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="card-header">
                        <h3 class="mb-0">Information</h3>
                    </div>
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <form action="{{ route('admin.user.edit',$user->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input type="text" class="form-control" name="address" value="{{$user->address}}">
                            </div>
                            <div class="form-group">
                                <label for="">Phone</label>
                                <input type="text" class="form-control" name="phone" value="{{$user->phone}}" required/>
                            </div>
                            <div class="row justify-content-end m-0">
                                <button type="submit"
                                        class="btn btn-primary button-submit">Update</button>
                                <a class="pl-2" href="{{route('admin.users')}}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="Can">
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection