@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><a href="{{ route('admin.users') }}"><i class="" aria-hidden="true"></i>User list</a></li>
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Create new user</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="card-header">
                        <h3 class="mb-0">Information</h3>
                    </div>
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <form action="{{route('admin.user-new.store')}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Name</label>
                                <span class="align-middle text-danger">*</span>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="">Pass</label>
                                <div class="input-group">
                                    <input type="password" class="form-control pwd" name="password" required>
                                    <span class="input-group-btn">
                                    <button class="btn btn-default reveal" type="button"><i class="fa fa-eye"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input type="text" class="form-control" name="address">
                            </div>
                            <div class="form-group">
                                <label for="">Phone</label>
                                <input type="text" class="form-control" name="phone" required/>
                            </div>
                            <div class="row justify-content-end m-0">
                                <button type="submit"
                                        class="btn btn-primary button-submit">Create</button>
                                <a class="pl-2" href="{{route('admin.users')}}">
                                    <input type="button"
                                           class=" btn btn-outline-secondary button-submit"
                                           value="Can">
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection