@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Users</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="card-header">
                        <a class="btn btn-primary float-left" href="{{route('admin.user-new')}}">Create</a>
                        </div>
                    @include('layouts.notification.success')
                    @include('layouts.notification.error')
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <table id="data-table" class="table">
                            <thead class="">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                            <tr>
                                <th scope="row">{{ ++$key }}</th>
                                <td>{{$user->name}}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td class="text-center">
                                    <div class="ml-auto">
                                        @if($user->id !== \App\Http\Controllers\UserConstant::ADMIN)
                                        <a class="pt-2 pl-3" href="{{route('admin.user.update', ['user'=>$user->id]) }}"><i class="fa fa-edit"></i>Edit </a>
                                        <a class="pt-2 pl-3" href="" data-toggle="modal" data-target="#modalDeleteUser"><i class="fa fa-close text-danger"></i>Delete</a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <form method="post" action="{{ route('admin.user.destroy', $user->id) }}">
        {{ csrf_field() }}
    <div class="modal fade" id="modalDeleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDeleteUser">Delete User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-danger">Are you sure you want to delete this user?</p>
                </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Delete</button>
                    </div>

            </div>
        </div>
    </div>
    </form>
@endsection