@extends('layouts.app')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Categories</li>
    <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Update</li>
</ol>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card row">
                <div class="card-header">
                    <h3 class="mb-0">Information</h3>
                </div>
                <div class="col-12 col-md-12 card-body table-responsive">
                    <form action="{{ route('admin.category.update', $category->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Parent</label>
                            <select class="form-control" name="cate-parent">
                                <option value="0">Please Choose Category</option>
                                <?php \App\Helpers\helpers::getCategoryParent($cateParents,0, "--", $category->parent_id) ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" value="{{$category->name}}" required>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="desc">{{$category->description}}</textarea>
                        </div>
                        <div class="row justify-content-end m-0">
                            <button type="submit"
                                    class="btn btn-primary button-submit">Update</button>
                            <a class="pl-2" href="{{ route('admin.categories.list') }}">
                                <input type="button"
                                       class=" btn btn-outline-secondary button-submit"
                                       value="Can">
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection