@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Categories</li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    <div class="card-header">
                        <a class="btn btn-primary float-left" href="{{route('admin.category-new')}}">Create</a>
                    </div>
                    @include('layouts.notification.success')
                    @include('layouts.notification.error')
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <table id="data-table" class="table">
                            <thead class="">
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Slug</th>
                                <th scope="col">Category Parent</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $key => $category)
                                <tr class="text-center">
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $category->name}}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>
                                        @php
                                            $cateParent = \App\Category::where('id',$category->parent_id)->first();
                                        @endphp
                                        @if($cateParent)
                                            {{ $cateParent->name }}
                                        @else
                                            {{ "None" }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="ml-auto">
                                            <a class="pt-2 pl-3" href="{{route('admin.category.edit',$category->id)}}"><i class="fa fa-edit"></i>Edit </a>
                                            <a class="pt-2 pl-3" href="{{route('admin.category.delete',$category->id)}}"><i class="fa fa-close text-danger"></i>Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection