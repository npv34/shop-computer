@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item text-muted"><i class="" aria-hidden="true"></i>Guests </li>
    </ol>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card row">
                    @include('layouts.notification.success')
                    @include('layouts.notification.error')
                    <div class="col-12 col-md-12 card-body table-responsive">
                        <table id="data-table" class="table">
                            <thead class="">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Address</th>
                                <th scope="col">Date Order</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($guests as $key => $guest)
                                <tr>
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $guest->first_name}} {{$guest->last_name}}</td>
                                    <td>{{ $guest->email }}</td>
                                    <td>{{ $guest->phone }}</td>
                                    <td>{{ $guest->address }}</td>
                                    <td>{{ $guest->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection