@extends('welcome')

@section('home-content')

<div id="maincontainer">
  <section id="product">
    <div class="container">
    <!--  breadcrumb -->  
      <ul class="breadcrumb">
        <li>
          <a href="{{ url('/') }}">Trang chủ</a>
          <span class="divider">/</span>
        </li>
        <li class="active">mua hàng</li>
      </ul>
      <div class="row">        
        <!-- Account Login-->
        @if (Session::has('success'))
          <p class="text-success">
            <i class="fa fa-check" aria-hidden="true"></i>
            {{ Session::get('success') }}
          </p>
        @endif
        <div class="col-lg-9">
          <h1 class="heading1">
            <span class="maintext">Mua hàng </span>
          </h1>
          <div class="checkoutsteptitle">Thông tin <a class="modify">Modify</a>
          </div>
          <div class="checkoutstep">
            <div class="row">
              <form class="form-horizontal" action="{{ route('user.storeCart') }}" method="post">
                {{ csrf_field() }}
                <fieldset>
                  <div class="col-lg-6">
                    <div class="control-group">
                      <label class="control-label" >Họ <span class="red">*</span></label>
                      <div class="controls col-md-9">
                        <input type="text" name="first_name" class="" value="{{old('first_name') }}" required>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" >Tên<span class="red">*</span></label>
                      <div class="controls">
                        <input type="text" class="" name="last_name"  value="{{old('last_name') }}" required>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" >E-Mail<span class="red">*</span></label>
                      <div class="controls">
                        <input type="email" class="" name="email_guest"  value="{{old('email_guest') }}" required>
                      </div>
                    </div>

                  </div>
                  <div class="col-lg-6">
                    <div class="control-group">
                      <label class="control-label" >Số điện thoại<span class="red">*</span></label>
                      <div class="controls">
                        <input type="text" class="" name="phone_guest"  value="{{old('phone_number') }}" required>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" >Địa chỉ <span class="red">*</span></label>
                      <div class="controls">
                        <input type="text" class="" name="address_guest"  value="{{old('address_guest')}}" required>
                      </div>
                    </div>
                  </div>
                </fieldset>
                <button type="submit" class="btn btn-orange pull-right">Gửi</button>
              </form>
            </div>

          </div>
          <div class="checkoutsteptitle">Giỏ hàng <a class="modify">Modify</a>
          </div>
          <div class="checkoutstep">
            <div class="cart-info">
              <form method="post" action="">
                {{ csrf_field() }}
              <table class="table table-striped table-bordered">
                <tr>
                  <th class="image">Ảnh </th>
                  <th class="name">Tên sản phẩm </th>
                  <th class="quantity">Số lượng </th>
                  <th class="price">Giá </th>
                  <th class="total">Thành tiền</th>
                </tr>
                @if(count($contentCart) > 0)
                  @foreach($contentCart as $item)
                <tr>
                  <td class="image"><a href="#"><img title="product" alt="product" src="{{ asset('storage/'. $item->options->img ) }}" height="50" width="50"></a></td>
                  <td  class="name"><a href="#">{{ $item->name }}</a></td>
                  <td class="quantity">
                    <p class="text-center">{{ $item->qty }}</p>
                  </td>
                  <td class="price">{{ number_format($item->price,0,",",".") }} VNĐ</td>
                  <td class="total">{{ number_format($item->price * $item->qty,0,",",".") }} VNĐ</td>
                </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="6" class="text-center"><p>Bạn chưa mua sản phẩm nào.</p></td>
                  </tr>
                  @endif
              </table>
              </form>
            </div>
            <div class="row">
                <div class="col-lg-6 pull-right">
                  <table class="table table-striped table-bordered ">
                    <tbody>
                      <tr>
                        <td>
                          <span class="extra bold totalamout">Tổng tiền:</span>
                          <p class="text-muted">Chưa bao gồm VAT 10%</p>
                        </td>
                        <td>
                          <span class="bold totalamout">{{ $total  }} VNĐ</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>        
        <!-- Sidebar Start-->
        <div class="col-lg-3">
          <aside>
            <div class="sidewidt">
              <h2 class="heading2"><span> Checkout Steps</span></h2>
              <ul class="nav nav-list categories">
                <li>
                  <a class="active" href="#">Checkout Options</a>
                </li>
                <li>
                  <a href="#">Billing Details</a>
                </li>
                <li>
                  <a href="#">Delivery Details</a>
                </li>
                <li>
                  <a href="#">Delivery Method</a>
                </li>                
                <li>
                  <a href="#"> Payment Method</a>
                </li>
              </ul>
            </div>
          </aside>
        </div>
        <!-- Sidebar End-->
      </div>
    </div>
  </section>
</div>

@endsection