
@extends('welcome')

@section('home-content')

<div id="maincontainer">
  <section id="product">
    <div class="container">
     <!--  breadcrumb --> 
      <ul class="breadcrumb">
        <li>
          <a href="#">Trang chủ </a>
          <span class="divider">/</span>
        </li>
        <li class="active"> Giỏ hàng </li>
      </ul>       
      <h1 class="heading1"><span class="maintext"> Giỏ hàng</span></h1>
      <!-- Cart-->
      <div class="cart-info">
        <form method="post" action="">
        <table class="table table-striped table-bordered">
          <tr>
                <th class="image">Ảnh </th>
                <th class="name">Tên sản phẩm </th>
                <th class="quantity">Số lượng </th>
                <th class="total">Hành động </th>
                <th class="price">Giá </th>
                <th class="total">Thành tiền </th>
          </tr>
                {{ csrf_field() }}
            @if(count($contentCart) > 0)
                @foreach($contentCart as $item)
                    <tr>
                        <td class="image">
                            <a href="#">
                                <img title="product" alt="product" src="{{ asset('storage/'. $item->options->img ) }}" height="50" width="50">
                            </a>
                        </td>
                        <td  class="name">
                            <a href="#">{{ $item->name }}</a>
                        </td>
                        <td class="quantity">
                            <input type="text" size="1" value="{{ $item->qty }}" name="quantity[40]" class="col-lg-1 qty">
                         </td>
                         <td class="total">
                             <a class="btn updateCart" id="{{ $item->rowId }}">
                                 <img class="tooltip-test" data-original-title="Update" src="{{asset('img/update.png')}}" alt="update">
                             </a>
                              <a href="{{ route('user.removeProcuctCart',$item->rowId) }}">
                                  <img class="tooltip-test" data-original-title="Remove"  src="{{asset('img/remove.png')}}" alt="remove">
                              </a>
                         </td>
                        <td class="price">{{ number_format($item->price,0,",",".") }} VNĐ </td>
                        <td class="total">{{ number_format($item->price * $item->qty,0,",",".") }} VNĐ</td>
                     </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6" class="text-center"><p>Bạn chưa mua sản phẩm nào.</p></td>
                </tr>
            @endif
        </table>
        </form>
      </div>
      <div class="container">
      <div>
          <div class="col-lg-5 pull-right">
            <table class="table table-striped table-bordered ">
              <tr>
                <td>
                    <span class="extra bold totalamout">Tổng tiền :</span>
                    <p class="text-muted">Chưa bao gồm VAT 10%</p>
                </td>
                <td><span class="bold totalamout">{{ $total  }} VNĐ</span></td>
              </tr>
            </table>
              <a href="{{ route('user.checkout') }}" class="btn btn-orange pull-right">CheckOut</a>
            <input type="submit" value="Continue Shopping" class="btn btn-orange pull-right mr10">
          </div>
        </div>
        </div>
    </div>
  </section>
</div>
@endsection
