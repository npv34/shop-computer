
@extends('welcome')

@section('home-content')
<div id="maincontainer">
  <section id="product">
    <div class="container">
     <!--  breadcrumb -->  
      <ul class="breadcrumb">
        <li>
          <a href="{{ url('/') }}">Trang chủ</a>
          <span class="divider">/</span>
        </li>
        <li class="active">{{ $category->name }}</li>
      </ul>
      <div class="row">        
        <!-- Sidebar Start-->
        <aside class="col-lg-3">
         <!-- Category-->  
          <div class="sidewidt">
            @if(count($menusCate) > 0)
            <h2 class="heading2"><span>Danh Mục </span></h2>
            <ul class="nav nav-list categories">
              @foreach($menusCate as $menuCate)
              <li>
                <a href="{{route('user.getCategory',[$menuCate->slug])}}">{{ $menuCate->name }}</a>
              </li>
              @endforeach
            </ul>
              @endif
          </div>
          <!-- Latest Product -->
          <div class="sidewidt">
            <h2 class="heading2"><span>Sản Phẩm Mới </span></h2>
            <ul class="bestseller">
              @foreach($productsLatest as $productLatest)
              <li>
                <img width="60" height="60" src="{{ ($productLatest->image) ? asset('storage/'.$productLatest->image) : '' }}" alt="product" title="product">
                <a class="productname" style="font-size: 12px" href=""> {{ $productLatest->name }}</a>
                <span class="price"> {{ number_format($productLatest->price,0,",",".")}} VND</span>
              </li>
              @endforeach
            </ul>
          </div>
        </aside>
        <!-- Sidebar End-->
        <!-- Category-->
        <div class="col-lg-9">          
          <!-- Category Products-->
          <section id="category">
               <!-- Sorting-->
                <div class="sorting well">
                  <form class=" form-inline pull-left">
                    Sort By :
                    <select>
                      <option>Default</option>
                      <option>Name</option>
                      <option>Pirce</option>
                    </select>
                  </form>
                  <div class="btn-group pull-right">
                    <button class="btn" id="list"><i class="icon-th-list"></i>
                    </button>
                    <button class="btn btn-orange" id="grid"><i class="icon-th icon-white"></i></button>
                  </div>
                </div>
               <!-- Category-->
                <section id="categorygrid">
                  <ul class="thumbnails grid">
                    @foreach($productsCategory as $productCategory)
                    <li class="col-lg-4 col-sm-6">
                      <a class="prdocutname" href="{{ route('user.getProductDetail',[$productCategory->category->slug,$productCategory->slug] ) }}">{{ $productCategory->name }}</a>
                      <div class="thumbnail">
                        @if($productCategory->promotion_price)
                        <span class="sale tooltip-test">Sale</span>
                        @endif
                        <a href="{{ route('user.getProductDetail',[$productCategory->category->slug,$productCategory->slug] ) }}"><img alt="" src="{{ ($productCategory->image) ? asset('storage/'.$productCategory->image) : '' }}"></a>
                        <div class="shortlinks">
                          <a class="details" href="#">DETAILS</a>
                          <a class="wishlist" href="#">WISHLIST</a>
                          <a class="compare" href="#">COMPARE</a>
                        </div>
                        <div class="pricetag">
                          <span class="spiral"></span><a href="{{ route('user.shopping',[$productCategory->id,$productCategory->slug]) }}" class="productcart">ADD TO CART</a>
                          <div class="price">
                            <div class="pricenew">{{ number_format($productCategory->promotion_price,0,",",".") }}</div>
                            <div class="priceold">{{ number_format($productCategory->price,0,",",".") }}</div>
                          </div>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                  <ul class="thumbnails list row">
                    @foreach($productsCategory as $productCategory)
                    <li>
                      <div class="thumbnail">
                        <div class="row">
                          <div class="col-lg-4 col-sm-4">
                            <span class="offer tooltip-test" >Offer</span>
                            <a href="#"><img alt="" src="{{ ($productCategory->image) ? asset('storage/'.$productCategory->image) : '' }}"></a>
                          </div>
                          <div class="col-lg-8 col-sm-8">
                            <a class="prdocutname" href="">{{ $productCategory->name }}</a>
                            <div class="productdiscrption"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.<br>
                              <br>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stan </div>
                            <div class="pricetag">
                              <span class="spiral"></span><a href="{{ route('user.shopping',[$productCategory->id,$productCategory->slug]) }}" class="productcart">ADD TO CART</a>
                              <div class="price">
                                <div class="pricenew">{{ number_format($productCategory->promotion_price,0,",",".") }}</div>
                                <div class="priceold">{{ number_format($productCategory->price,0,",",".") }}</div>
                              </div>
                            </div>
                            <div class="shortlinks">
                              <a class="details" href="#">DETAILS</a>
                              <a class="wishlist" href="#">WISHLIST</a>
                              <a class="compare" href="#">COMPARE</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                  <div>
                    <ul class="pagination pull-right">
                      {{ $productsCategory->links() }}
                    </ul>
                  </div>
                </section>
          </section>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection
