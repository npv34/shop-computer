@extends('welcome')

@section('home-content')
<div id="maincontainer">
  <section id="product" style="margin-top: 20px">
    <div class="container">      
      <!-- Product Details-->
      <div class="row">
       <!-- Left Image-->
        <div class="col-lg-5">
          <ul class="thumbnails mainimage">
            <li class="span5">
              <a  rel="position: 'inside' , showTitle: false, adjustX:-4, adjustY:-4" class="thumbnail cloud-zoom" href=" {{ ($product->image) ? asset('storage/'.$product->image) : '' }}">
                <img src="{{ ($product->image) ? asset('storage/'.$product->image) : '' }}" alt="" title="">
              </a>
            </li>
            @foreach($product->images as $imageDetail)
            <li class="span5">
              <a  rel="position: 'inside' , showTitle: false, adjustX:-4, adjustY:-4" class="thumbnail cloud-zoom" href="{{ ($imageDetail->image) ? asset('storage/'.$imageDetail->image) : '' }}">
                <img  src="{{ ($imageDetail->image) ? asset('storage/'.$imageDetail->image) : '' }}" alt="" title="">
              </a>
            </li>
            @endforeach
          </ul>
          <span>Mouse move on Image to zoom</span>
          <ul class="thumbnails mainimage">
            <li class="producthtumb" >
              <a class="thumbnail" >
                <img width="60"  src="{{ ($product->image) ? asset('storage/'.$product->image) : '' }}" alt="" title="">
              </a>
            </li>
            @foreach($product->images as $imageDetail)
            <li class="producthtumb" >
              <a class="thumbnail" >
                <img width="60"  src="{{ ($imageDetail->image) ? asset('storage/'.$imageDetail->image) : '' }}" alt="" title="">
              </a>
            </li>
            @endforeach
          </ul>
        </div>
         <!-- Right Details-->
        <div class="col-lg-7">
          <div class="row">
            <div class="col-lg-12">
              <h1 class="productname"><span class="bgnone">{{ $product->name }}</span></h1>
              <div class="productprice">

                @if($product->promotion_price)
                <div class="product-promotion_price">
                  <h3 class="text-danger">Khuyến mãi :{{ number_format($product->promotion_price,0,",",".") }} VNĐ</h3>
                </div>
                <div class="productpageoldprice">Giá cũ: {{ number_format($product->price,0,",",".") }} VNĐ</div>
                @else
                  <div class="product-promotion_price">
                    <h3 class="text-danger">Giá bán: {{ number_format($product->price,0,",",".") }} VNĐ</h3>
                  </div>
                @endif

                <ul class="rate">
                  <li class="on"></li>
                  <li class="on"></li>
                  <li class="on"></li>
                  <li class="off"></li>
                  <li class="off"></li>
                </ul>
              </div>
              <ul class="productpagecart">
                <li><a class="cart" href="{{ route('user.shopping',[$product->id,$product->slug]) }}">ADD TO CART</a>
                </li>
              </ul>
         <!-- Product Description tab & comments-->
              <div class="productdesc">
                    <ul class="nav nav-tabs" id="myTab">
                      <li class="active"><a href="#description">Tổng quan</a>
                      </li>
                      <li><a href="#specification">Thông số kỹ thuật </a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="description">
                        <h2>{{ $product->name }}</h2>
                        {!!  $product->description !!}
                        <br>
                      </div>
                      <div class="tab-pane " id="specification">
                          {!!  $product->content  !!}
                      </div>
                    </div>
              </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--  Related Products-->
  <section id="related" class="row">
    <div class="container">
      <h1 class="heading1"><span class="maintext">Sản Phẩm Liên Quan </span></h1>
      <ul class="thumbnails">
        @foreach($productsRelate as $productRelate)
        <li class="col-lg-3 col-sm-3">
          <a class="prdocutname" href="product.blade.php">{{ $productRelate->name }}</a>
          <div class="thumbnail">
            @if($productRelate->promotion_price)
            <span class="sale tooltip-test">Sale</span>
            @endif
            <a href="#"><img alt="" src="{{ ($productRelate->image) ? asset('storage/'.$productRelate->image) : '' }}"></a>
            <div class="shortlinks">
              <a class="details" href="#">DETAILS</a>
              <a class="wishlist" href="#">WISHLIST</a>
              <a class="compare" href="#">COMPARE</a>
            </div>
            <div class="pricetag">
              <span class="spiral"></span><a href="#" class="productcart">ADD TO CART</a>
              <div class="price">
                @if($product->promotion_price)
                  <div class="pricenew">{{ number_format($product->promotion_price,0,",",".") }}</div>
                  <div class="priceold">{{ number_format($product->price,0,",",".") }}</div>
                  @else
                  <div class="pricenew">{{ number_format($product->price,0,",",".") }}</div>
                  @endif
              </div>
            </div>
          </div>
        </li>
        @endforeach
      </ul>
    </div>
  </section>
  <!-- Popular Brands-->
  <section id="popularbrands" class="container">
    <h1 class="heading1"><span class="maintext">Thương hiệu nổi tiếng </span></h1>
    <div class="brandcarousalrelative">
      <ul id="brandcarousal">
        <li><img src="{{asset('img/brand1.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand2.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand3.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand4.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand1.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand2.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand3.jpg')}}" alt="" title=""/></li>
        <li><img src="{{asset('img/brand4.jpg')}}" alt="" title=""/></li>
      </ul>
      <div class="clearfix"></div>
      <a id="prev" class="prev" href="#">&lt;</a>
      <a id="next" class="next" href="#">&gt;</a>
    </div>
  </section>
</div>

@endsection

