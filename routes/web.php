<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;


Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::prefix('/admin')->group(function () {
        Route::get('/', 'HomeController@index')->name('admin.home');
        //Route User
        Route::prefix('users')->group(function () {
            Route::get('/', 'UserController@showList')->name('admin.users');
            Route::get('/new', 'UserController@create')->name('admin.user-new');
            Route::post('/new', 'UserController@store')->name('admin.user-new.store');
            Route::get('/{user_id}/edit', 'UserController@update')->name('admin.user.update');
            Route::post('/{user_id}/edit', 'UserController@edit')->name('admin.user.edit');
            Route::post('/{user_id}/delete', 'UserController@destroy')->name('admin.user.destroy');
        });

        //Route Category
        Route::prefix('categories')->group(function () {
            Route::get('/new', 'CategoryController@create')->name('admin.category-new');
            Route::post('/new', 'CategoryController@store')->name('admin.category-new.store');
            Route::get('/{cate_id}/edit', 'CategoryController@edit')->name('admin.category.edit');
            Route::post('/{cate_id}/edit', 'CategoryController@update')->name('admin.category.update');
            Route::get('/', 'CategoryController@showList')->name('admin.categories.list');
            Route::get('/{cate_id}/delete', 'CategoryController@delete')->name('admin.category.delete');
            Route::post('/{cate_id}/delete', 'CategoryController@destroy')->name('admin.category.destroy');
        });
//        //Route Products
        Route::prefix('products')->group(function () {
            Route::get('/new', 'ProductController@create')->name('admin.product.new');
            Route::post('/new', 'ProductController@store')->name('admin.product.store');
            Route::get('/', 'ProductController@showList')->name('admin.products.list');
            Route::get('/{product_id}/delete', 'ProductController@delete')->name('admin.products.delete');
            Route::post('/{product_id}/delete', 'ProductController@destroy')->name('admin.products.destroy');
            Route::get('/{product_id}/update', 'ProductController@update')->name('admin.products.update');
            Route::post('/{product_id}/update', 'ProductController@edit')->name('admin.products.edit');
        });
        //Route Guests
        Route::prefix('guests')->group(function () {
            Route::get('/', 'ShoppingCartController@showGuests')->name('admin.showGuests');
        });
        //Route Bills
        Route::prefix('bills')->group(function () {
            Route::get('/', 'ShoppingCartController@showBills')->name('admin.showBills');
            Route::get('/{id}/detail', 'ShoppingCartController@showBillDetail')->name('admin.showBillDetail');

            Route::post('/{id}/update', 'ShoppingCartController@updateBill')->name('admin.updateBill');

            Route::post('/{id}/delete', 'ShoppingCartController@destroyBill')->name('admin.destroyBill');
        });
    });

});


Route::prefix('/')->group(function () {

    //route shopping cart
    Route::get('/shopping-cart' ,'WelcomeController@showCart')->name('user.showCart');

    Route::get('/remove-product/{id}' ,'WelcomeController@removeProductCart')->name('user.removeProcuctCart');

    Route::get('/update-product/{id}/{qty}' ,'WelcomeController@updateProductCart')->name('user.updateProcuctCart');

    Route::get('/check-out' ,'WelcomeController@checkout')->name('user.checkout');

    Route::post('/store-cart' ,'ShoppingCartController@storeCart')->name('user.storeCart');

    //index

    Route::get('/', 'WelcomeController@index')->name('user.home');

    Route::get('{nameCategory}', 'WelcomeController@getCategory')->name('user.getCategory');

    Route::get('{nameCategory}/{nameProduct}', 'WelcomeController@getProductDetail')->name('user.getProductDetail');

    Route::get('shopping/{id}/{nameProduct}' ,'WelcomeController@shopping')->name('user.shopping');

});










